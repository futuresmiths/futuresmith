## Future Smith

Future Smith is an volunteering company that empowers you the people to contribute and create your own projects and tribes

<b>Visit us at <url>http://futuresmith.com.au</url></b>

This project is built using the laravel framework and this source code is released under the GNU General Public License v3.0 license 

###Installation

1. Install composer <url>https://getcomposer.org</url>
2. Clone this repository `git clone https://github.com/oasisers99/futuresmith.git`
3. You will need to generate your env file
    * Create a .env file (on some systems this will be hidden. You have to enable hidden files)
 
    ```php
       # Run This command
       php artisan key:generate
    ```
    * Add the result as `APP_KEY=<YOUR KEY HERE>` to your .env file
    * Configure the rest of your .env file based off the options in the config/app.php file
        * APP_NAME = Application Name
        * APP_ENV = Application Environment (i.e. local, production)
        * APP_URL = The URL of the application
        
4. Open the additional resources folder and run the script to create your database
    * We use MYSQL for our database.
    
5. Now you are ready!
    ```php
       # Run This command
       php artisan serve
    ```
    * Visit on your host (read your console from the previous command for location of website)


###Developers
* Minseok - https://github.com/oasisers99
* Filip Blazevski - https://github.com/fblazevski



